# Timing Data

This repo contains the raw data and processing scripts for the timings
  presented in the paper [Practical EMV Relay Protection](practical_emv.gitlab.io), section VI and Appendices
  * `processing_scripts` -- python scripts used to calculate averages, standard
    deviations and correlations of timings, based on the traces in the
    `raw_traces` folder; the script for generating plots of the timings is also 
    given;
  * `raw_traces` -- the Proxmark traces acquired by sniffing the reader-card
    transaction communication; it is organised by card type (Mastercard,
    Mastercard RRP, Visa) and within each of those files the logs are organised
    by distance and angle;
  * `acr-readers` -- the code for `libnfc`, with our two programs for emulating
    an EMV reader running the Mastercard protocol and the Visa protocol;

## Usage:

### `ACR-readers`
- the folder contains the code of the `libnfc` repository
- the `examples` folder contains the two required files to run the Mastercard and 
  Visa protocols
- compiling the repository requires `autoconf`:

```sh
autoconf
./configure
make all
```

- the executable wrapper scripts should then appear in the `examples` folder
- the `loop_*.sh` files were used to run the reader programs multiple times

### Timings data and scripts

#### Average and standard deviation
- computes average and standard deviation for a folder containing raw Proxmark
  traces 
- assumes folder structure of the format `[distance]mm/[angle]`
- `-s` option generates `*.tim` files to be used with the correlation and
  plotting scripts
- `-f` option refers to APDUs which are broken up due to either Reader or
  Card limited buffers; setting the flag will calculated the timing
  for a **whole** APDU, even if it was split into chunks

```sh
usage: run_avg_std.py [-h] [-s] [-f] foldername

positional arguments:
  foldername  folder to parse files from

optional arguments:
  -h, --help  show this help message and exit
  -s          print summary to file
  -f          if messages are broken up, only the first chunk will be considered if the flag is not provided; the *whole* message is considered if the flag is passed
```

#### Correlation
- computes the correlation between distances and reply times
- assumes the existence of `*.tim` files within each distance/angle
  directory (generated with the average/stdev script)
  
```sh
usage: correlation.py [-h] [-d] foldername

positional arguments:
  foldername  folder to parse files from

optional arguments:
  -h, --help  show this help message and exit
  -d          enable debug
```

#### Plotting
- generates plots for averages or standard deviations for the collected
  timings
- assumes the existence of `*.tim` files within each distance/angle
  directory (generated with the average/stdev script)
- `-l` parameter is a label that will be included in the title (e.g. card type)
- `-o` parameter specifies if the script should generate plots for averages or 
  standard deviations; if it is not supplied, it generates **both**
- `-c` parameter specifies which command to generate the plot for; command
  names are expected to match the dictionaries in the script file; if it
  is not supplied, it generates plots for **all** the commands
- `-f` parameter specifies the folder to store the generated pdfs; assumes
  the folder already exists; if not supplied, it outputs them in the current
  directory

```sh
usage: plot.py [-h] [-l L] [-o O] [-c C] [-f F] [-d] foldername

positional arguments:
  foldername  folder to parse files from

optional arguments:
  -h, --help  show this help message and exit
  -l L        label to be added to the file names
  -o O        option: avg or std
  -c C        command to generate plot for
  -f F        folder to store pdfs in
  -d          enable debug
```

