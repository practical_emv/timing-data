import numpy as np
import scipy.stats as ss

import glob
import pprint
import argparse
import os

import math

import statistics

DEBUG = True

summary_avg = {}
summary_std = {}
dist_idxes = {}
angle_idxes = {}
pp = pprint.PrettyPrinter(indent=4)
output_folder = ''


parse_results = {}

def parse_logfile(filename, distances):
    global parse_results

    parse = {}
    file = open(filename, 'r')
    filename = filename.split('/')[-1][:-4]
    distance = str(float(filename.split('_')[1][:-2])) + 'mm'
    angle = filename.split('_')[2]

    while True: 
    
        # Get next line from file 
        line = file.readline() 
    
        # if line is empty 
        # end of file is reached 
        if not line or len(line) == 0: 
            break
        (cmd, time) = (line.strip()).split('|')
        if cmd not in parse:
            parse[cmd] = []
        parse[cmd].append(float(time))
    file.close()

    if DEBUG: print(distance, angle)

    for _cmd, times in parse.items():
        if DEBUG: print(_cmd, times)
        if not len(times) == 0:
            if _cmd not in parse_results:
                parse_results[_cmd] = {}
                for _dist in distances:
                    if _dist not in parse_results[_cmd]:
                      parse_results[_cmd][_dist] = []
            parse_results[_cmd][float(distance[:-2])].extend(parse[_cmd])


def pearsonr_ci(x,y,alpha=0.05):

    coeff, pval = ss.pearsonr(x,y)
    r_z = np.arctanh(coeff)
    se = 1/np.sqrt(x.size-3)
    z = ss.norm.ppf(1-alpha/2)
    lo_z, hi_z = r_z-z*se, r_z+z*se
    lo, hi = np.tanh((lo_z, hi_z))
    return coeff, pval, lo, hi

def run_stats(cmd):
    global parse_results

    data = parse_results[cmd]
    
    dist_list = []
    time_list = []
    for dist, time in data.items():
        for _ in range(len(time)):
            dist_list.append(dist)
        time_list.extend(time)

    ## create new arrays
    dist_array = np.array(dist_list)
    time_array = np.array(time_list)

    coeff, pval, lo, hi = pearsonr_ci(dist_array, time_array)
    print(f'{cmd:>20} | {coeff:>12.5f} | {pval} | {lo:>15.7f} | {hi:>15.7f} |')



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("foldername", help="folder to parse files from")
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()


    DEBUG = args.d
    ## handle subfolders
    folders = [x[0] for x in os.walk(args.foldername)]

    ## get all timing files
    logfiles = []
    for _folder in folders:
        for file in glob.glob(_folder + "/*.tim"):
            logfiles.append(file)

    ## angles and distances
    angles = []
    distances = []

    for _file in logfiles:
        filename = _file.split('/')[-1][:-4]
        split_name = filename.split('_')
        _d = float(split_name[1][:-2])
        if _d not in distances:
            distances.append(_d)
        _a = int(split_name[2])
        if _a not in angles:
            angles.append(_a)

    if DEBUG: print(angles)
    if DEBUG: print(distances)

    # prep the indexes
    distances.sort()
    angles.sort()
    idx = 0
    for _dist in distances:
        dist_idxes[str(_dist) + 'mm'] = idx
        idx += 1

    idx = 0
    for _ang in angles:
        angle_idxes[str(_ang)] = idx
        idx += 1

    if DEBUG: print(dist_idxes, angle_idxes)

    for _file in logfiles:
        filename = _file.split('/')[-1][:-4]
        split_name = filename.split('_')
        _a = int(split_name[2])
        if _a in angles:
            parse_logfile(_file, distances)

    ## remove unused commands
    to_remove = []
    for cmd, dist_dict in parse_results.items():
        _empty = False
        for dist, time_list in dist_dict.items():
            if len(time_list) == 0:
                _empty = True
        if _empty:
            to_remove.append(cmd)

    for _cmd in to_remove:
        _r = parse_results.pop(_cmd, 'None')
        if DEBUG: print("Removed", _cmd, _r)


    ## generate stats

    for _cmd in parse_results.keys():
        run_stats(_cmd)