

import argparse
import sys 
import glob
import pprint 
import statistics
import os

US = 0.073746312684366

seqence_cmds = { \
    '26': 'wupa', \
    '5000': 'halt', \
    '9320': 'anticoll', \
    '9370': 'select_uid', \
    'e050bca5' : 'rats', \
    '00a404000e32': 'Select_2Pay', \
    '00a4040007': 'Select_AID', \
    '80a80000': 'GPO_1', \
    '80ae9000': 'GPO_2', \
    'f2019140': 'Wait', \
    '80ea0000041112131400': 'rrp_1', \
    '80ea0000041112131500': 'rrp_2', \
    '80ea0000041112131600': 'rrp_3', \
    '00b2011400': 'read_record:_01,_SFI:_2', \
    '00b2011c00': 'read_record:_01,_SFI:_3', \
    '00b2012400': 'read_record:_01,_SFI:_4', \
    '00b2022400': 'read_record:_02,_SFI:_4', \
    '00b2022c00': 'read_record:_02,_SFI:_5', \
    '00b2032400': 'read_record:_03,_SFI:_4', \
    '80ae50004200000': 'gen_AC', \
    '802a8e800401020304': 'checksum', \
    'c2e0b4': 'restore'
    }

parse_results = {
    'wupa': [], \
    'halt': [], \
    'anticoll': [], \
    'select_uid': [], \
    'rats': [], \
    'Select_2Pay': [], \
    'Select_AID': [], \
    'GPO_1': [], \
    'GPO_2': [], \
    'Wait': [], \
    'rrp_1': [], \
    'rrp_2': [], \
    'rrp_3': [], \
    'read_record:_01,_SFI:_2': [], \
    'read_record:_01,_SFI:_3': [], \
    'read_record:_01,_SFI:_4': [], \
    'read_record:_02,_SFI:_4': [], \
    'read_record:_02,_SFI:_5': [], \
    'read_record:_03,_SFI:_4': [], \
    'gen_AC': [], \
    'checksum': [], \
    'restore': [], \
    'unknown': []
    }

lenghts = {
    'wupa': [], \
    'halt': [], \
    'anticoll': [], \
    'select_uid': [], \
    'rats': [], \
    'Select_2Pay': [], \
    'Select_AID': [], \
    'GPO_1': [], \
    'GPO_2': [], \
    'Wait': [], \
    'rrp_1': [], \
    'rrp_2': [], \
    'rrp_3': [], \
    'read_record:_01,_SFI:_2': [], \
    'read_record:_01,_SFI:_3': [], \
    'read_record:_01,_SFI:_4': [], \
    'read_record:_02,_SFI:_4': [], \
    'read_record:_02,_SFI:_5': [], \
    'read_record:_03,_SFI:_4': [], \
    'gen_AC': [], \
    'checksum': [], \
    'restore': [], \
    'unknown': []
    }

def first_message_chunk(lines, trace_matrix):
    _cont_card = False
    _cont_read = False
    for _line in lines:
        if not '+' in _line:
            columns = _line.split('|')
            if len(columns) > 1:     
                if (columns[0].strip()).isnumeric() or columns[0].strip() == '':
                    data = columns[3].replace(' ', '')
                    data = data.replace("'", '')
                    data = data.replace('26(7)', '26')
                    columns[3] = data.upper()
                    if columns[2].strip() != '':
                        trace_matrix.append(columns)
                    else:
                        _elem = trace_matrix.pop()
                        _elem[3] += columns[3]
                        _elem[4] = columns[4]
                        _elem[5] = columns[5]
                        trace_matrix.append(_elem)
    return trace_matrix

def full_message_parsing(lines, trace_matrix):
    _cont_card = False
    _cont_read = False
    for _line in lines:
        if not '+' in _line:
            columns = _line.split('|')
            if len(columns) > 1:
                if (columns[0].strip()).isnumeric() or columns[0].strip() == '':
                    data = columns[3].replace(' ', '')
                    data = data.replace("'", '')
                    data = data.replace('26(7)', '26')
                    columns[3] = data.upper()
                    if _cont_card:
                        if 'Rdr' in columns[2] and not ("a2e6d7".upper() == columns[3] or "a36fc6".upper() == columns[3]):
                            _cont_card = False
                            trace_matrix.append(columns)      
                        else:
                            _elem = trace_matrix.pop()
                            _elem[1] = columns[1]
                            _elem[3] += columns[3]
                            _elem[4] = columns[4]
                            _elem[5] = columns[5]
                            trace_matrix.append(_elem)
                            _cont_card = False
                    elif _cont_read:
                        if 'Tag' in columns[2] and not ("a2e6d7".upper() == columns[3] or "a36fc6".upper() == columns[3]):
                            _cont_read = False
                            trace_matrix.append(columns)
                        else:
                            _elem = trace_matrix.pop()
                            _elem[1] = columns[1]
                            _elem[3] += columns[3]
                            _elem[4] = columns[4]
                            _elem[5] = columns[5]
                            trace_matrix.append(_elem)
                            _cont_read = False
                    else: 
                        if columns[2].strip() != '':
                            if "Rdr" in columns[2] and ("a2e6d7".upper() == columns[3] or "a36fc6".upper() == columns[3]):
                                _cont_card = True
                            elif "Tag" in columns[2] and ("a2e6d7".upper() == columns[3] or "a36fc6".upper() == columns[3]):
                                _cont_read = True
                            else:
                                trace_matrix.append(columns)
                        else:
                            _elem = trace_matrix.pop()
                            _elem[3] += columns[3]
                            _elem[4] = columns[4]
                            _elem[5] = columns[5]
                            trace_matrix.append(_elem)
    return trace_matrix

def parse_trace(raw_trace, chunks):
    global parse_results
    global lenghts
    prev_cmd = ''
    prev_source = ''
    ## The exclamation mark in data means parity error --
    ## trace might need to be redone
    lines = raw_trace.splitlines()
    trace_matrix = []

    if chunks:
        trace_matrix = full_message_parsing(lines, trace_matrix)
    else:
        trace_matrix = first_message_chunk(lines, trace_matrix)
    
    ## columns 0 and 1 are start/end timestamps 
    ## column 2 is the source of the message (tag/reader)
    ## column 3 is the data
    ## column 4 is crc status
    ## column 5 is annotation

    to_remove = []

    ## remove waits; it doens't affect our timing measurements
    for _r in range(len(trace_matrix)):
        for k, v in seqence_cmds.items():
            if (trace_matrix[_r][3].lower()).startswith(k):
                found = True
                if 'Wait' in v:
                    to_remove.append(trace_matrix[_r])

    for _elem in to_remove:
        trace_matrix.remove(_elem)
    
    rdr_start = 0
    for _r in range(len(trace_matrix)):
        
        if 'Rdr' in trace_matrix[_r][2]:
            found = False
            if '!' in trace_matrix[_r][3] or '!' in trace_matrix[_r][5]:
                print('trace error')
            for k, v in seqence_cmds.items():
                if (trace_matrix[_r][3].lower()).startswith(k) or (trace_matrix[_r][3][2:].lower()).startswith(k):
                    found = True
                    prev_cmd = v
                    rdr_start = int(trace_matrix[_r][0])
                    prev_source = 'Rdr'
                    break
            if not found:
                if 'rats' in prev_cmd:
                    prev_cmd = 'Select 2Pay'
                else:
                    prev_cmd = 'unknown'
            rdr_start = int(trace_matrix[_r][0])
            prev_source = 'Rdr'
        elif 'Tag' in trace_matrix[_r][2]:
            ## some reply
            if 'Tag' not in prev_source:
                tag_end = int(trace_matrix[_r][1])
                parse_results[prev_cmd].append((tag_end - rdr_start)*US)
                lenghts[prev_cmd].append(len(trace_matrix[_r][3])/2)
                prev_source = 'Tag'
            else:
                print('Two consecutive tag commands')
            prev_source = 'Tag'

def clear_dicts():
    global parse_results
    global lenghts
    for k, v in parse_results.items():
        v *= 0 
    for k, v in lenghts.items():
        v *= 0


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("foldername", help="folder to parse files from")
    parser.add_argument("-s", action='store_true', help="print summary to file")
    parser.add_argument("-f", action='store_true', help="if messages are broken up, only the first chunk will be considered if the flag is not provided; the *whole* message  is considered if the flag is passed")
    args = parser.parse_args()

    ## handle subfolders
    folders = [x[0] for x in os.walk(args.foldername)]
    print(folders)

    
    for _folder in folders:
        logfiles = []
        for file in glob.glob(_folder + "/*.log"):
            logfiles.append(file)
        if len(logfiles) != 0:
            pp = pprint.PrettyPrinter(indent=4)
            print('*'*20)
            print("Files:")
            pp.pprint(logfiles)
            print('*'*20)
            
            for _file in logfiles:
                with open(_file, 'r') as f:
                    raw_trace = f.read()
                    parse_trace(raw_trace, args.f)               
                
            if args.s:
                split_folder = _folder.split('/')
                with open(_folder + '/summary' + '_' + split_folder[-2] + '_' + split_folder[-1] + '.tim', 'w') as f:
                    for k, v in parse_results.items():
                        for _item in v:
                            f.write(f'{k:>30}|  {_item:20.3f} \n')

            print("How many transactions (counts of WUPA):", len(parse_results["wupa"]))
            print("Timings:")
            header = f'{"command":>30}| {"avg":>15}| {"std":>15}| {"bytes":>7}| {"min":>15}| {"max":>15}| {"diff":>15}|'
            print(header)
            print('-' * 74)
            for k, v in parse_results.items():
                if not len(v) == 0:
                    avg = sum(v)/len(v)
                    if len(v) > 1:
                        stdev = statistics.stdev(v)
                    else:
                        stdev = -1
                    avg_len = sum(lenghts[k])/len(lenghts[k])
                    item = f'{k:>30}| {avg:>15.2f}| {stdev:>15.2f}| {int(avg_len):>7}| {max(v):>15.2f}| {min(v):15.2f}| {max(v)-min(v):>15.2f}|'
                    print(item)


        clear_dicts()
