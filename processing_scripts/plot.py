from mpl_toolkits.mplot3d.axes3d import Axes3D

import matplotlib.pyplot as plt
import numpy as np

import glob
import pprint
import argparse
import os

import math

import statistics

from matplotlib import rcParams
rcParams['font.family'] = 'serif'

DEBUG = False

summary_avg = {}
summary_std = {}
dist_idxes = {}
angle_idxes = {}
pp = pprint.PrettyPrinter(indent=4)
output_folder = ''

## should be enough colours for each distance
colours = [(0.95,1.00,0.22), (1.00,0.67,0.00), (0.23,0.70,0.00), (0.00,0.60,0.90), (0.73,0.20,1.00), (0.70,0.23,0.00), (0.00,0.40,0.33), (0.00,0.00,0.70), (0.25,0.00,0.30)]


def prep_summary(len_dist, len_angles):
    global summary_avg
    global summary_std

    summary_avg = {
    'wupa': np.zeros(shape=(len_angles, len_dist)), \
    'halt': np.zeros(shape=(len_angles, len_dist)), \
    'anticoll': np.zeros(shape=(len_angles, len_dist)), \
    'select_uid': np.zeros(shape=(len_angles, len_dist)), \
    'rats': np.zeros(shape=(len_angles, len_dist)), \
    'Select_2Pay': np.zeros(shape=(len_angles, len_dist)), \
    'Select_AID': np.zeros(shape=(len_angles, len_dist)), \
    'GPO_1': np.zeros(shape=(len_angles, len_dist)), \
    'GPO_2': np.zeros(shape=(len_angles, len_dist)), \
    'Wait': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_1': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_2': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_3': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_2': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_3': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_02,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_02,_SFI:_5': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_03,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'gen_AC': np.zeros(shape=(len_angles, len_dist)), \
    'checksum': np.zeros(shape=(len_angles, len_dist)), \
    'restore': np.zeros(shape=(len_angles, len_dist)), \
    'unknown': np.zeros(shape=(len_angles, len_dist))
    }

    summary_std = {
    'wupa': np.zeros(shape=(len_angles, len_dist)), \
    'halt': np.zeros(shape=(len_angles, len_dist)), \
    'anticoll': np.zeros(shape=(len_angles, len_dist)), \
    'select_uid': np.zeros(shape=(len_angles, len_dist)), \
    'rats': np.zeros(shape=(len_angles, len_dist)), \
    'Select_2Pay': np.zeros(shape=(len_angles, len_dist)), \
    'Select_AID': np.zeros(shape=(len_angles, len_dist)), \
    'GPO_1': np.zeros(shape=(len_angles, len_dist)), \
    'GPO_2': np.zeros(shape=(len_angles, len_dist)), \
    'Wait': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_1': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_2': np.zeros(shape=(len_angles, len_dist)), \
    'rrp_3': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_2': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_3': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_01,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_02,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_02,_SFI:_5': np.zeros(shape=(len_angles, len_dist)), \
    'read_record:_03,_SFI:_4': np.zeros(shape=(len_angles, len_dist)), \
    'gen_AC': np.zeros(shape=(len_angles, len_dist)), \
    'checksum': np.zeros(shape=(len_angles, len_dist)), \
    'restore': np.zeros(shape=(len_angles, len_dist)), \
    'unknown': np.zeros(shape=(len_angles, len_dist))
    }


def gen_plot(option, command, card_type, distances, angles):

    fig = plt.figure(figsize=[5, 5.3])
    ax = fig.add_subplot(111, projection='3d')
    # fig.subplots_adjust(left=0, bottom=0, top=0.7, wspace=0, hspace=0)
    plt.margins(0.0)
    if DEBUG: print("distances", distances)
    if 'avg' in option:
        hist = summary_avg[command]
    elif 'std' in option:
        hist = summary_std[command]
    _min = min(hist[0])
    _max = 0
    
    ## get minimum
    for _row in hist:
        if min(_row) < _min:
            _min = min(_row)
        if max(_row) > _max:
            _max = max(_row)

    if DEBUG: print("min", _min, "max", _max)

    ## substract min
    for _row in range(len(hist)):
        for _cell in range(len(hist[_row])):
            hist[_row][_cell] = hist[_row][_cell] - _min

    if DEBUG: print(hist)


    xedges = np.array([_ for _ in range(len(angles))])
    yedges = np.array([_ for _ in range(len(distances))])
    if DEBUG: 
        print('xedges', xedges)
        print('yedges', yedges)

    xpos, ypos = np.meshgrid(xedges + 0.25, yedges + 0.25, indexing="ij")
    xpos = xpos.ravel()
    ypos = ypos.ravel()
    zpos = 0

    if DEBUG: 
        print('xpos', xpos)
        print('ypos', ypos)
        print('zpos', zpos)

    # Construct arrays with the dimensions for the 16 bars.
    dx = dy = 0.5 * np.ones_like(zpos)
    dz = hist.ravel()
    if DEBUG: print('hist', hist)

    if DEBUG: 
        print("dx", dx)
        print("dy", dy)
        print("dz", dz)
        print(len(xpos), len(ypos), len(dz))

    plt.xticks(np.arange(0.25, 0.25+len(angles), 1), labels=angles, fontsize=8)

    ticksy = np.arange(0.75, len(distances)+0.75, 1)
    plt.yticks(ticksy, distances, fontsize=8)

    ax.set_xlabel('Angles (deg)')
    ax.set_ylabel('Distance (mm)')
    ax.set_zlabel('Time (us)', labelpad=20)


    ## tikz y location steps
    diff = _max-_min

    ticksz = np.arange(0, diff, diff/5)
    ## n labels
    step = (_max-_min)/(len(ticksz))
    if DEBUG: print("step", step)
    zticks = [f'{_:5.2f}' for _ in np.arange(_min, _max, step)]
    # zticks = 
    ax.tick_params(axis="z",direction="in", pad=10)
    if DEBUG: 
        print(zticks)
        zzzticks = ax.get_zticks()
        print("zzz ticks", zzzticks)
    ax.set_zticks(ticksz)
    ax.set_zticklabels(zticks, fontsize=8)

    colour_array = []

    for _ang in range(len(angles)):
        colour_array.extend(colours[:len(distances)])

    if DEBUG: print(len(colour_array), colour_array)

    if 'avg' in option:
        plt.title(card_type + ' ' + command.replace('_', ' ') + ' average')
    elif 'std' in option:
        plt.title(card_type + ' ' + command.replace('_', ' ') + ' standard dev')

    ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color=colour_array)

    # plt.show()
    if 'avg' in option:
        plt.savefig(output_folder + command + '_avg_' + card_type + '.pdf', bbox_inches="tight")
    elif 'std' in option:
        plt.savefig(output_folder + command + '_std_' + card_type + '.pdf', bbox_inches="tight")

def parse_logfile(filename, angles):
    ## rows are distances
    ## columns are degrees
    global summary_avg
    global summary_std

    parse = {}
    file = open(filename, 'r')
    filename = _file.split('/')[-1][:-4]
    distance = str(float(filename.split('_')[1][:-2])) + 'mm'
    angle = filename.split('_')[2]

    if int(angle) in angles:
    
        while True: 
        
            # Get next line from file 
            line = file.readline() 
        
            # if line is empty 
            # end of file is reached 
            if not line: 
                break
            (cmd, time) = (line.strip()).split('|')
            if cmd not in parse:
                parse[cmd] = []
            parse[cmd].append(float(time))
        file.close()

        for k, v in parse.items():
            if not len(v) == 0:
                avg = sum(v)/len(v)
                if len(v) > 1:
                    stdev = statistics.stdev(v)
                else:
                    stdev = -1
            
            summary_avg[k][angle_idxes[angle]][dist_idxes[distance]] = avg
            summary_std[k][angle_idxes[angle]][dist_idxes[distance]] = stdev


def gen_all_plots(option, cmd, card_type, distances, angles):
    len_dist = len(distances)
    len_angles = len(angles)

    
    # for k in opt.keys():
    if option is None:
        ## averages plots
        if cmd is None:
            for command in summary_avg.keys():
                print('avg', command)
                if np.count_nonzero(summary_avg[command]) > 0:
                    gen_plot('avg', command, card_type, distances, angles)
                else:
                    print(command, 'is empty')

            ## st dev plots
            for command in summary_std.keys():
                print("std", command)
                if np.count_nonzero(summary_std[command]) > 0:
                    gen_plot('std', command, card_type, distances, angles)
                else:
                    print(command, 'is empty')
        else:
            print('avg', cmd)
            if np.count_nonzero(summary_avg[cmd]) > 0:
                gen_plot('avg', cmd, card_type, distances, angles)
            else:
                print(cmd, 'is empty')
            print('std', cmd)
            if np.count_nonzero(summary_std[cmd]) > 0:
                gen_plot('std', cmd, card_type, distances, angles)
            else:
                print(cmd, 'is empty')
    elif 'avg' in option:
        if cmd is None:
            for command in summary_avg.keys():
                print('avg', command)
                if np.count_nonzero(summary_avg[command]) > 0:
                    gen_plot('avg', command, card_type, distances, angles)
                else:
                    print(command, 'is empty')
        else:
            print('avg', cmd)
            if np.count_nonzero(summary_avg[cmd]) > 0:
                gen_plot('avg', cmd, card_type, distances, angles)
            else:
                print(cmd, 'is empty')
    elif 'std' in option:
        if cmd is None:
            for command in summary_std.keys():
                print("std", command)
                if np.count_nonzero(summary_std[command]) > 0:
                    gen_plot('std', command, card_type, distances, angles)
                else:
                    print(command, 'is empty')
        else:
            print("std", cmd)
            if np.count_nonzero(summary_std[cmd]) > 0:
                gen_plot('std', cmd, card_type, distances, angles)
            else:
                print(cmd, 'is empty')
    else: 
        print("Option {} not valid!" % option)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("foldername", help="folder to parse files from")
    parser.add_argument("-l", help="label to be added to the file names")
    parser.add_argument("-o", help="option: avg or std")
    parser.add_argument("-c", help="command to generate plot for")
    parser.add_argument("-f", help="folder to store pdfs in")
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()

    if not args.f is None:
        output_folder = args.f
        if not args.f[-1] == '/':
            output_folder += '/'

    DEBUG = args.d
    ## handle subfolders
    folders = [x[0] for x in os.walk(args.foldername)]

    ## get all timing files
    logfiles = []
    for _folder in folders:
        for file in glob.glob(_folder + "/*.tim"):
            logfiles.append(file)

    ## angles and distances
    angles = []
    distances = []

    for _file in logfiles:
        filename = _file.split('/')[-1][:-4]
        split_name = filename.split('_')
        _d = float(split_name[1][:-2])
        if _d not in distances:
            distances.append(_d)
        _a = int(split_name[2])
        if _a not in angles:
            angles.append(_a)

    if DEBUG: print(angles)
    if DEBUG: print(distances)

    # prep the indexes
    distances.sort()
    angles.sort()

    idx = 0
    for _dist in distances:
        dist_idxes[str(_dist) + 'mm'] = idx
        idx += 1

    idx = 0
    for _ang in angles:
        angle_idxes[str(_ang)] = idx
        idx += 1

    if DEBUG: print(dist_idxes, angle_idxes)

    prep_summary(len(distances), len(angles))

    for _file in logfiles:
        parse_logfile(_file, angles)

    gen_all_plots(args.o, args.c, args.l, distances, angles)
